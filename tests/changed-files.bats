load common

@test "Changed-files lists the changed files" {
  local DBFILE WATCHDIR

  # Prepare
  DBFILE="${BATS_TMPDIR}/database.sqlite3"
  WATCHDIR="${BATS_TMPDIR}/files_to_watch"
  create_watched_files_1 "${WATCHDIR}"

  ${TEST_EXECUTABLE} -d "${DBFILE}" -f "${WATCHDIR}" create-revision
  run ${TEST_EXECUTABLE} -d "${DBFILE}" changed-files 1

  # Check
  [ "$output" = "${WATCHDIR}/onetwothree" ]
  [ "$status" -eq 0 ]

  # Prepare
  create_watched_files_2 "${WATCHDIR}"

  ${TEST_EXECUTABLE} -d "${DBFILE}" create-revision
  run ${TEST_EXECUTABLE} -d "${DBFILE}" changed-files 2

  # Check
  [ "$output" = "${WATCHDIR}/fourfivesix" ]
  [ "$status" -eq 0 ]
}

@test "Changed-files lists deleted files" {
  local DBFILE WATCHDIR

  # Prepare
  DBFILE="${BATS_TMPDIR}/database.sqlite3"
  WATCHDIR="${BATS_TMPDIR}/files_to_watch"
  create_watched_files_1 "${WATCHDIR}"

  ${TEST_EXECUTABLE} -d "${DBFILE}" -f "${WATCHDIR}" create-revision
  run ${TEST_EXECUTABLE} -d "${DBFILE}" changed-files 1

  # Check
  [ "$output" = "${WATCHDIR}/onetwothree" ]
  [ "$status" -eq 0 ]

  rm "${WATCHDIR}/onetwothree"

  ${TEST_EXECUTABLE} -d "${DBFILE}" create-revision
  run ${TEST_EXECUTABLE} -d "${DBFILE}" changed-files 2

  # Check
  [ "$output" = "${WATCHDIR}/onetwothree" ]
  [ "$status" -eq 0 ]
}
