# Set the command that will be tested.
if [ "" = "${TEST_EXECUTABLE}" ]; then
  export TEST_EXECUTABLE="changed_files"
fi

create_watched_files_1 () {
  local WATCHDIR
  WATCHDIR="$1"

  if [ ! -d "${WATCHDIR}" ]; then
    mkdir ${WATCHDIR}
  fi
  echo 123 > ${WATCHDIR}/onetwothree
}

create_watched_files_2 () {
  local WATCHDIR
  WATCHDIR="$1"

  if [ ! -d "${WATCHDIR}" ]; then
    mkdir ${WATCHDIR}
  fi
  echo 456 > ${WATCHDIR}/fourfivesix
}

create_watched_files_3 () {
  local WATCHDIR
  WATCHDIR="$1"

  if [ ! -d "${WATCHDIR}" ]; then
    mkdir ${WATCHDIR}
  fi
  echo 789 > ${WATCHDIR}/seveneightnine
}

change_existing_files_1 () {
  local WATCHDIR
  WATCHDIR="$1"

  if [ ! -d "${WATCHDIR}" ]; then
    echo "Watched directory must exist to change existing files." 2>&1
    exit 1
  fi
  echo 123 >> onetwothree
  echo 456 >> fourfivesix
  echo 789 >> seveneightnine
}

teardown () {
  if [ "${BATS_TMPDIR}" != "" ] && [ -f "${BATS_TMPDIR}/database.sqlite3" ]; then
    rm ${BATS_TMPDIR}/database.sqlite3
  fi
  if [ "${BATS_TMPDIR}" != "" ] && [ -d "${BATS_TMPDIR}/files_to_watch" ]; then
    rm -rf ${BATS_TMPDIR}/files_to_watch
  fi
}
