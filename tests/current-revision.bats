load common

@test "Current-revision reports the current revision" {
  local DBFILE WATCHDIR

  # Prepare
  WATCHDIR="${BATS_TMPDIR}/files_to_watch"
  create_watched_files_1 "${WATCHDIR}"

  DBFILE="${BATS_TMPDIR}/database.sqlite3"
  ${TEST_EXECUTABLE} -d "${DBFILE}" -f "${WATCHDIR}" create-revision
  run ${TEST_EXECUTABLE} -d "${DBFILE}" current-revision

  # Check
  [ "$output" = "1" ]
  [ "$status" -eq 0 ]

  # Prepare
  create_watched_files_2 "${WATCHDIR}"

  ${TEST_EXECUTABLE} -d "${DBFILE}" create-revision
  run ${TEST_EXECUTABLE} -d "${DBFILE}" current-revision

  # Check
  [ "$output" = "2" ]
  [ "$status" -eq 0 ]
}
