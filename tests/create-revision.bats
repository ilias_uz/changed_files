load common

@test "Create-revision creates a database the first time it is run" {
  local DBFILE WATCHDIR

  # Prepare
  WATCHDIR="${BATS_TMPDIR}/files_to_watch"
  create_watched_files_1 "${WATCHDIR}"

  DBFILE=${BATS_TMPDIR}/database.sqlite3
  run ${TEST_EXECUTABLE} -d "${DBFILE}" -f "${WATCHDIR}" create-revision

  # Check
  [ -f "${DBFILE}" ]
  [ "$output" == "" ]
  [ "$status" -eq 0 ]
}

@test "Create-revision must not create a new revision when no files change" {
  local DBFILE E WATCHDIR

  # Prepare
  WATCHDIR=${BATS_TMPDIR}/files_to_watch
  create_watched_files_1 "${WATCHDIR}"

  DBFILE=${BATS_TMPDIR}/database.sqlite3
  ${TEST_EXECUTABLE} -d "${DBFILE}" -f "${WATCHDIR}" create-revision
  run ${TEST_EXECUTABLE} -d "${DBFILE}" -f "${WATCHDIR}" create-revision

  # Check
  E="No files have been changed.  Cowardly refusing to create empty revision."
  [ "$output" == "${E}" ]
  [ "$status" -eq 1 ]
}

@test "Support deleting files" {
  local DBFILE E WATCHDIR

  WATCHDIR=${BATS_TMPDIR}/files_to_watch
  create_watched_files_1 "${WATCHDIR}"

  DBFILE=${BATS_TMPDIR}/database.sqlite3
  ${TEST_EXECUTABLE} -d "${DBFILE}" -f "${WATCHDIR}" create-revision
  rm ${WATCHDIR}/onetwothree
  run ${TEST_EXECUTABLE} -d "${DBFILE}" create-revision
  [ "$output" == "" ]
  [ "$status" -eq 0 ]
}

@test "Create-revision understands the BACKUP_TOOLS_DB environment variable" {
  local DBFILE WATCHDIR

  # Prepare
  WATCHDIR="${BATS_TMPDIR}/files_to_watch"
  create_watched_files_1 "${WATCHDIR}"

  DBFILE=${BATS_TMPDIR}/database.sqlite3
  export BACKUP_TOOLS_DB="${DBFILE}"
  run ${TEST_EXECUTABLE} -f "${WATCHDIR}" create-revision
  unset BACKUP_TOOLS_DB

  # Check
  [ -f "${DBFILE}" ]
  [ "$output" == "" ]
  [ "$status" -eq 0 ]
}
