load common

@test "Running without a command shows help" {
  local beginning cmd
  cmd=${TEST_EXECUTABLE}
  run ${cmd}
  beginning=$(echo $output | sed -e '1!d' -e "s/${cmd}.*\$/${cmd}/")
  [ "$beginning" = "Usage: ${cmd}" ]
  [ "$status" -eq 0 ]
}

@test "The -h and --help options show help" {
  local beginning cmd
  cmd=${TEST_EXECUTABLE}
  run ${cmd} -h
  beginning=$(echo $output | sed -e '1!d' -e "s/${cmd}.*\$/${cmd}/")
  [ "$beginning" = "Usage: ${cmd}" ]
  [ "$status" -eq 0 ]
  run ${cmd} --help
  beginning=$(echo $output | sed -e '1!d' -e "s/${cmd}.*\$/${cmd}/")
  [ "$beginning" = "Usage: ${cmd}" ]
  [ "$status" -eq 0 ]
}

@test "The required commands are present in the help" {
  local changed_files create_revision current_revision
  run ${TEST_EXECUTABLE} --help

  changed_files=$(
    echo "$output" | grep changed-files > /dev/null && echo found)
  create_revision=$(
    echo "$output" | grep create-revision > /dev/null && echo found)
  current_revision=$(
    echo "$output" | grep current-revision > /dev/null && echo found)
  [ "$changed_files" = "found" ]
  [ "$create_revision" = "found" ]
  [ "$current_revision" = "found" ]
  [ "$status" -eq 0 ]
}
